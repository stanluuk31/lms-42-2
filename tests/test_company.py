import re
import flask


def add_company(client, login, name):
    client.login("teacher1")

    return client.post("/internships/add", data=dict(
        name=name,
        login=login,
    ), follow_redirects=True)


def company_login(client, email):
    client.post("/user/login", data=dict(
        email=email,
    ))

    with open("/tmp/lms42-email.txt") as file:
        data = file.read()
        print("data", data)
        login_path = re.search(r"https?://[^/]+(/.*)", data).group(1)
        return client.get(login_path, follow_redirects=True)


def test_add_company(client):
    name="Example company"
    login="example.com"

    response = add_company(client, login, name)
    assert name.encode('utf-8') in response.data


def test_invalid_login(client):
    name="Example company"
    login="correct@example.com"

    response = add_company(client, login, name)
    assert name.encode('utf-8') in response.data

    email="wrong@example.com"
    response = client.post("/user/login", data=dict(
        email=email,
    ))

    assert b"This address is not registered." in response.data
    assert b"Company profile" not in response.data



def test_company_login(client):
    name="Example company"
    login="example.com"

    response = add_company(client, login, name)
    assert name.encode('utf-8') in response.data

    email="representative@example.com"
    response = company_login(client, email)

    assert b"Successfully logged in as representative of Example company" in response.data
    assert b"Company profile" in response.data


def test_authorization(client):
    name="Example company"
    login="example.com"

    response = add_company(client, login, name)
    assert name.encode('utf-8') in response.data

    client.post("/user/logout")

    response = client.get("/internships/4/add_team", follow_redirects=True)
    assert b"Submit" not in response.data
    assert b"Send login link" in response.data

    response = client.get("/internships/edit/4", follow_redirects=True)
    assert b"Save" not in response.data
    assert b"Send login link" in response.data

    company_login(client, "test@example.com")

    response = client.get(f"/internships/4/add_team", follow_redirects=True)
    assert b"Submit" in response.data

    response = client.get("/internships/edit/4", follow_redirects=True)
    assert b"Save" in response.data