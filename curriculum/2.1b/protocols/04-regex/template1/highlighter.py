import sys
import re
import keyword
from blessed import Terminal

# Read the input file. We just let the program crash if no input file
# or a non-existing one has been specified.
if len(sys.argv) != 2:
    print("Provide a .py file name as an argument")
    sys.exit(1)
with open(sys.argv[1]) as file:
    data = file.read()

# The regular expression. It should match all of the file, while matching
# recognized parts of syntax in corresponding capture groups named 'string',
# 'comment', 'keyword', 'identifier' and 'number' respectively.
matcher = re.compile(rf'''
(?P<number>[0-9]+) # TODO objective #1: Extend to match other numbers.
| # The regex should match either one of the types, so we're using the '|' (or) operator.
(?P<keyword>todo) # TODO objective #2
|
# TODO objective #3-#6
[\w\W] # This matches any single character that doesn't match any of the above types.
''', re.MULTILINE | re.VERBOSE)


# Initialize blessed, for colored terminal output
term = Terminal()

# A dict that maps syntax parts to their output colors
colors = {
    "string": term.darkgoldenrod1,
    "identifier": term.cornflowerblue,
    "keyword": term.chartreuse2,
    "comment": term.dimgray,
    "number": term.fuchsia,
}

# Iterate all regex matches
for match in matcher.finditer(data):
    # See if for this match, any of the capture groups is set
    for name, func in colors.items():
        try:
            if match.group(name) != None:
                # It is set! Use the function from the dict to print the match in the right color
                print(func(match.group()), end="")
                break
        except IndexError:
            pass
    else:
        # No capture groups found. Just print the match (in the default color, white).
        print(match.group(), end="")
