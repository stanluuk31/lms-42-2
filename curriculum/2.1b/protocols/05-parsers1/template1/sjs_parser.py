import sjs_tree
import sys
import traceback
from typing import NoReturn


class Parser:
    def __init__(self, tokens):
        self.__tokens = tokens
        self.__current_token_pos = 0
        self.__expected_tokens = [] # The list of things the current token did *not* match (used for error reporting).
    
    @property
    def token(self):
        """Get the current token. This is a @property, so use `self.token` instead of `self.token()`."""
        return self.__tokens[self.__current_token_pos]

    def next_token(self):
        """Progress to the next token."""
        self.__expected_tokens = []
        self.__current_token_pos += 1

    def match_kind(self, kind):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directly. It returns the current token *if* it matches the given `kind`, or None otherwise.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the kind is added to `expected_tokens`, which helps `error` give
        a better message."""
        if self.token.kind == kind:
            token = self.token
            self.next_token()
            return token
        self.__expected_tokens.append(f"<{kind}>")
        return None

    def match_text(self, text):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directly. It returns the current token *if* it matches the given `text`, or None otherwise.
        `text` may also be a list of options to match.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the kind is added to `expected_tokens`, which helps `error` give
        a better message."""
        texts = text if isinstance(text, list) else [text]
        if self.token.text in texts:
            token = self.token
            self.next_token()
            return token
        self.__expected_tokens += [repr(text) for text in texts]
        return None

    def error(self) -> NoReturn:
        """Display an error message, indicating the line, column, text and kind of the current token and
        the list of tokens that were expected instead (from `self.expected_tokens`), and exits the program.
        """
        traceback.print_stack(file=sys.stdout)
        print(f"Expected {' or '.join(self.__expected_tokens)} at line {self.token.line} column {self.token.column}, but found {repr(self.token)}.")
        sys.exit(1)

    def require(self, val):
        """A utility function that may be convenient instead of checking for errors manually. It
        exits with an error if `val` is `None` or `False`, or just returns `val` otherwise.
        """
        if val is False or val is None:
            self.error()
        return val

    def parse(self):
        """The main parse method. It returns a Program object, or exits with an error.
        It should be called only once on an instance.

        <Program> ::= <Statements>
        """
        statements = self.parse_statements()
        # Make sure that after parsing all statements that we can, we have arrived at the
        # end-of-file marker. (If not, that would indicate code at the end.)
        self.require(self.match_kind('eof'))
        return sjs_tree.Program(statements)

    def parse_statements(self):
        """<Statements> ::= ( <Statement> )*"""
        statements = []
        while True:
            statement = self.parse_statement()
            if not statement:
                # We have reached the end of the statements.
                return sjs_tree.Block(statements)
            statements.append(statement)

    def parse_statement(self):
        """<Statement> ::= <Declaration> ';' | <Expression> ';' | <IfStatement> | <WhileStatement>"""
        statement = self.parse_declaration() or self.parse_expression()
        if statement:
            # declaration or expression require an additional semicolon
            self.require(self.match_text(';'))
            return statement
        # Returns None if nothing was matched.
        return self.parse_if() or self.parse_while()

    def parse_block(self):
        """<Block> ::= <Statement> | '{' <Statements> '}'"""
        # TODO: Return a single statement, or a list of statements.

        # Returns None if nothing was matched.
        return None

    def parse_declaration(self):
        """<Declaration> ::= 'var' IDENTIFIER ( '=' <Expression> )?"""
        if not self.match_text("var"):
            # Returns None if nothing was matched.
            return None
        
        identifier = self.require(self.match_kind('identifier'))

        # TODO: Optional initialization
        expression = None
    
        return sjs_tree.Declaration(identifier.text, expression)

    def parse_expression(self):
        """<Expression> ::= <Atom> ( OPERATOR <Expression> )?"""
        # TODO / Return a sjs_tree.BinaryOperator(left, operator_token.text, right)

        # Return None if nothing was matched.
        return None
    
    def parse_atom(self):
        """<Atom> ::= "(" <Expression> ")" | INTEGER | STRING | IDENTIFIER ( "(" ( <Arguments> )? ")" )?"""

        # TODO: Expression between parenthesis.
        # TODO: Integer literal: sjs_tree.Literal(int(token.text))
        
        token = self.match_kind('string')
        if token:
            return sjs_tree.Literal(token.text[1:len(token.text)-1]) # strip the quotes
        
        # TODO: Variable reference: sjs_tree.VariableReference(token.text)
        # TODO: Function call: sjs_tree.FunctionCall(token.text, args)
        #       Hint: use self.parse_arguments() for this.

        # Return None if none of the above were matched.
        return None

    def parse_arguments(self):
        """<Arguments> ::= <Expression> ( "," <Arguments> )?"""
        # TODO: Return a list of expressions.

    def parse_if(self):
        """<IfStatement> ::= "if" "(" <Expression> ")" <Block> ( "else" <Block> )?"""
        if self.match_text("if"):
            # TODO: Return a sjs_tree.IfStatement(condition, yes, no)
            pass
        # Return None if nothing was matched.
        return None

    def parse_while(self):
        """<WhileStatement> ::= "while" "(" <Expression> ")" <Block>"""
        # TODO: Return a sjs_tree.WhileStatement(condition, block)
        # Return None if nothing was matched.
        return None
