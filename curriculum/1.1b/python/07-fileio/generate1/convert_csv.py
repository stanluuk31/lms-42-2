import csv

def convert(rows):
    converted = []
    for row in rows:
        converted_row = []
        for cell in row:
            cell = cell.replace("\n", " ")
            converted_row.append(f"<{cell}>")
        converted.append("".join(converted_row))
    return converted

books_csv = open('books_hacking.csv', encoding='utf-8-sig')
book_reader = csv.reader(books_csv, delimiter=';')
book_catalog = convert(book_reader)

with open('books_hacking.catalog', 'w') as f:
    f.writelines("\n".join(book_catalog))
