- |
    Considering that you're a fresh student just starting out on a degree programme, there's only one possible application we could possibly have you built as your first real programming assignment. Yes, a responsible alcohol use advisor app, of course!
    
    You'll be creating the application step-by-step, but here is a demo of how the final product should work:

    <script>
        renderAsciinema('alcohol.cast');
    </script>

- |
    *Note:* If you already have experience programming in another language, Python may feel weird at first. It has its own conventions, like `snake_case` for variables and functions (instead of `camelCase` or `TitleCase`), *not* using `(`braces`)` around `if` conditions, and *not* using semicolons`;` after each statement. As a multi-lingual programmer, you should get used to learning and sticking to the conventions of the programming environment you're currently working in.

-
    title: Welcome!
    ^merge: feature
    code: 0
    text: |
        All great things start with a small first step.

        Within the `alcohol.py` file, create a program that just prints the following output:

        ```

        *** Responsible alcohol use calculator ***

        ```

        Notice the empty lines before and after the title.

        *Hint:* You'll require the use of `print` and a `"string"`. Note that strings can be empty: `""`.

-
    title: Who's this?
    ^merge: feature
    code: 0
    text: |
        Extend your program to ask the user's name, and to say *hi* to her or him. The output should look something like this:

        ```

        *** Responsible alcohol use calculator ***

        What's you name? Frank
        Hi Frank
        ```

        When the user enters a name different than *Frank* that name should of course be used to say *hi* to.

        *Hint:* You'll require the use of `input` and a variable. Make sure to give your variable a fitting name. Remember that `print` can have multiple arguments, separated by commas, to output multiple things on one line.

-
    link: https://www.youtube.com/watch?v=2aA3VBdcl6A
    title: PY4E - Conditionals (Chapter 3 Part 1)
    info: Conditionals and indentation blocks. Don't worry about the spaces versus tabs discussion, as Visual Studio Code will automatically do the right thing. You can press `tab` to *indent* and `shift-tab` to *dedent* the current line. This also works for multiple lines when you have them selected.

-
    title: Gender?
    ^merge: feature
    code: 0
    text: |
        Extend your program to ask the user's gender. Print a womanly message when the user enters 'female' and print a manly message when the user enters 'male'. For now, don't do anything when the user enters something else.

        *Hint:* You'll require the use of `if` and the `==` operator (for comparison).

-
    title: Drinks?
    ^merge: feature
    code: 0
    text: |
        Have your program ask for the weekly number of alcoholic beverages the user drinks on average. Then show how many beverage that are per year.

        ```

        *** Responsible alcohol use calculator ***

        What's you name? Frank
        Hi Frank!
        What's your gender? [female/male] male
        Men are awesome!
        How many alcoholic beverage do you drink on average per week? 10
        That's 520 per year!
        ```

        For now, your application may just crash (quit with an error message) when the user enters anything other than a whole number.

        *Hint:* You'll require `int` to convert a string to an integer, in order to do the `*` calculation.

-
    link: https://www.youtube.com/watch?v=OczkNrHPBps
    title: PY4E - Conditionals (Chapter 3 Part 2)
    info: Introducing `elif` and `else`. You can stop watching when he starts talking about `try` and `except` for now.

-
    title: A word of advice
    ^merge: feature
    code: 0
    text: |
        - When the user drinks 0 alcoholic beverages per week, output the text `Healthy choice, <NAME>!`.
        - Otherwise, if the user drinks less than 7 alcoholic beverages per week, output the text `You'll probably be okay, <NAME>!`.
        - Otherwise output the text `Drinking less would be a healthier choice, <NAME>!`.

        *Hint:* You'll require the use of `elif` and `else`.

-
    title: Gender-specific advice
    ^merge: feature
    code: 0
    text: |
        Instead of just saying `Drinking less would be a healthier choice`, we want to add some more stringent advice, but [the norms](https://www.trimbos.nl/kennis/alcohol-in-cijfers/cijfers-alcoholgebruik-volwassenen) are based on the gender of the drinker.
        
        - For women, the norm for excessive drinking is 14 beverages per week.
        - For men, it's 21 beverages per week.

        Based on that:

        - When the user doesn't drink more than the norm, we'll stick to the messages from the previous assignment.
        - Otherwise, when the user doesn't drink more than twice the norm, we want to output `You're an excessive drinker, <NAME>.` `That's not healthy.`.
        - If the user drinks even more, we want to output `You're a really heavy drinker, <NAME>.` `That's very unhealthy.` `Change your ways!`

        *Hint:* Write the norm for the selected gender to a (fittingly named) variable, assigning it the appropriate value in the `if`-block for gender. Next, you can use that variable to extend the `if/elif/else` construct from the previous objective.

-
    link: https://www.youtube.com/watch?v=OczkNrHPBps#t=6m4s
    title: PY4E - Conditionals (Chapter 3 Part 2)
    info: Now watch the second part of this video, introducing `try` and `except`.

-
    title: Error handling
    ^merge: feature
    code: 0
    text: |
        Users rarely behave the way we programmers think they should. They've been know to type the biggest nonsense into your carefully crafted application! It's up to us programmers to deal with all of that.

        In each of these cases, the program should display a respectful error message and then stop the program:

        - When the user leaves the name field empty.
        - When the user enters a gender other than `male` or `female`.
        - When the user enters a beverage count that is not a whole number (integer).

        After doing this, your program should be unbreakable (semi-Dutch: *hufterproof*).

        *Hints:*

        - You can call the `exit()` function to stop your Python program early (before it reaches the end).
        - For the last check, you can use `try` and `except` to catch the error thrown by the `int` function when you pass it a string that does not contain a proper integer number. Try to keep the code block you surround with `try` and `except` as small as possible.

-
    title: Punctuality
    must: true
    text: |
        Attention to detail is very important in your way to become an awesome developer. Make sure that the output of your program:

        - Closely matches the provided video and example outputs.
        - Contains no spelling errors. *Hint*: Install the *Code Spell Checker* extension (by *streetsidesoftware*) for Visual Studio Code. Just take a minute to do this. Really.
        - Uses correct and easy to understand messages.
        - Uses capital letters, white spaces and punctuation in all the right places.

-   
    title: Code quality
    text: Your code should be easy to understand, using the simplest possible logic, sensible variable names, empty lines to separate distinct parts of the program and consistent use of whitespace.
    ^merge: codequality
    weight: 1.5
