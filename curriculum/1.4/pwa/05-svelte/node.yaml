name: Svelte
description: Create a declarative/reactive user interface using Svelte.
goals:
  svelte: 1
days: 2
resources:
    -
        link: https://www.youtube.com/watch?v=rv3Yq-B8qp4
        title: Svelte in 100 seconds
        info: You are not expected to understand all of what is said here, but for those that are confused/disappointed/angry about why we're not learning React/Vue/Angular (or whatever other framework you might already know or have heard of), this should provide you with some reasons.
    - |
        *Note:* In this assignment, you'll be working with Svelte, *not* SvelteKit. The difference in short: the former is just a frontend framework, while the latter also provides the backend and more bells and whistles. Make sure you don't unintentionally fall into a SvelteKit rabbit hole when searching the web for more information!
    -
        link: https://www.youtube.com/watch?v=uK2RnIzrQ0M&t=23s
        title: Svelte Crash Course
        info: A half an hour video introduction to Svelte. If you don't like it, you can also skip ahead to the tutorial below.
    -
        link: https://svelte.dev/tutorial/basics
        title: Svelte Tutorial
        info: |
            This interactive tutorial should help you get started with Svelte. If you watched the video, you can go over some parts quickly. Although you can skip the interactive bits by clicking *Show me*, that may not be the best way to learn. We recommend you study the following chapters:

            | Chapter | What to study |
            | --- | --- |
            | 1 | All |
            | 2 | All |
            | 3 | All |
            | 4 | All |
            | 5 | a..c |
            | 6 | a..f, m |
            | 7 | - |
            | 8 | a..c, f |
            | 9 | - |
            | 10 | - |
            | 11 | - |
            | 12 | - |
            | 13 | All |
            | 14 | - |
            | 15 | - |
            | 16 | - |
            | 17 | - |
            | 18 | - |
            | 19 | - |

    -
        link: https://svelte.dev/docs
        title: Svelte Documentation
        info: |
            The official docs. It's very readable, full of examples, and all on one page for easy searching!

assignment:
    Introduction:
        - |
            Create an extended version of the previous assignment's ToDo app, this time based on Svelte. As this is a pretty different way of creating and updating user interfaces, it's probably better *not* to copy any code from the previous assignment. Except maybe the code you used for communicating with the API. 

        -
            title: Demo video
            link: https://video.saxion.nl/media/t/1_40uhlf7h
            info: To get an idea what to aim for, you can watch this video. It has no audio.

    Getting started: |
        The provided template contains two Node.js project: a backend project and a frontend project. The former serves the API on port 42511, while the latter serves the Svelte front-end on port 5000.

        To start the backend from within your project directory:

        ```
        cd backend
        npm ci
        npm start
        ```

        Install the `svelte` plugin for VisualStudio Code.

        To start the frontend from within your project directory:

        ```
        cd frontend
        npm ci
        npm start
        ```
        
        The frontend contains quite a bit of code to get you started. You're encourages to study this code well, before you start editing it.

    Non-functional requirements:
        -
            must: true
            text: |
                - Use Svelte for all user interface creation and manipulation.
                - Event handlers should only change the app state variables. DOM changes must be handled by Svelte.
                - The user-interface should be the same as in the demo video, or clearly better. To that end, you can use the provided `style.css` file, and have a peek in the `static-html-examples` directory. 
                - At least 5 Svelte Components should be used. This shouldn't be hard, as a basis for these is already provided by the template.
        -
            ^merge: feature
            weight: 10
            text: All changes (except switching tabs) should persist to the database using the provided REST API. Connection errors don't need to be handled. When reloading the site (on a different device) the state should be restored.
        -
            ^merge: feature
            weight: 10
            text: All changes (except maybe item and list creation) should be reflected in the user-interface immediately, without waiting for a round-trip to the server. To help diagnose this, the provided REST API adds a 1s delay to all operations that should not require a round-trip before the UI is updated..

    Functional requirements:
        -
            ^merge: feature
            weight: 40
            text: Implement all of the functional requirements from the previous assignment.
        -
            ^merge: feature
            weight: 5
            text: Tabs should always be ordered alphabetically.
        -
            ^merge: feature
            weight: 5
            text: The add/edit item modal should have an extra `<select>` element, for setting the item priority to low, medium or high.
        -
            ^merge: feature
            weight: 5
            text: Items in a ToDo-list are grouped by priority. First high, then medium, then low. Within a priority group, items are ordered by name.
        -
            ^merge: feature
            weight: 5
            text: Above each priority group, there is a label ('High', 'Medium' or 'Low'). Labels for empty priority groups are not shown.
        -
            ^merge: feature
            weight: 5
            text: A loading indicator (or message) should be shown while retrieving the lists and while retrieving the items. 
        
        - |
            Consider how you would implement these additions in the previous assignment, without Svelte. Would your code still be maintainable?

    Non-requirements: |
        - Animations (such as in the video) are not required. If you want to look into them anyway, you should explore the Svelte tutorial a bit further.
        - You are not required to handle errors caused by multiple users/browsers interacting with your server simultaneously.
        - Connection errors don't need to be handled either.

    REST API documentation: |
        This is exactly the same API as in the previous assignment, with the addition of a `priority` property.

        ## Get all lists
        `GET /api/lists`

        Status code 200. Response body example:
        ```js
        [
            {id: 1, name: "Groceries"},
            {id: 2, name: "Apollo Project"}
        ]
        ```

        ## Create a new list
        `POST /api/lists`

        Returns the newly created item including the id that was assigned to it.

        Request body example:
        ```js
        {name: "WebTech"}
        ```
        Status code 201. Response body example:
        ```js
        {id: 3, name: "WebTech"}
        ```
        Status code 400. Response body example:
        ```js
        {error: "Invalid name"}
        ```

        ## Delete a list
        `DELETE /api/lists/:listId`
        
        Status code 200. Response body example:
        ```js
        {}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such list"}
        ```

        ## Get all items for a list
        `GET /api/lists/:listId/items`

        Status code 200. Response body example:
        ```js
        [
            {id: 1, name: "Sugar", priority: 'medium', checked: false},
            {id: 2: name: "Milk", priority: 'high', checked: true}
        ]
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such list"}
        ```
        

        ## Create a new item for a list
        `POST /api/lists/:listId/items`

        Returns the newly created item including its assigned id.

        Request body example:
        ```js
        {name: "Build a spaceship", priority: 'low'}
        ```
        Status code 201. Response body example:
        ```js
        {id: 1, name: "Build a spaceship", priority: 'low', checked: false}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such list"}
        ```
        Status code 400. Response body example:
        ```js
        {error: "Invalid name"}
        ```

        
        ## Delete an item
        `DELETE /api/lists/:listId/items/:itemId`
        
        Status code 200. Response body example:
        ```js
        {}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such item"}
        ```


        ## Change properties of an item
        `PUT /api/lists/:listId/items/:itemId`

        Returns the new state of the item.

        Request body example:
        ```js
        {checked: true, name: "Build a HUGE spaceship"}
        ```
        Any combination of `checked`, `name` and `priority` can be specified. Unspecified properties will be kept at their current value.

        Status code 200. Response body example:
        ```js
        {id: 1, name: "Build a HUGE spaceship", priority: 'low', checked: true}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such item"}
        ```
