- |
    Let's build something that resembles a Philips Hue hub: a thing that controls smart home lights. Light come in two variants, white lights that only have a *light level* (between 0 and 100, 0 meaning off), and color lights that have *hue* and *saturation* as well.

    It should be able to set the state of individual lights, add these lights into *rooms*, and create *scenes* for such rooms, allowing you to easily save the states of all lights in the room, give this state a name (eg. 'dinner' or 'movie' or 'romantic'), and activate it at a later time.

    You only need to create a code *library* (so no user interface) for this, and you won't have to actually interact with lighting hardware - instead we'll just verify that your lights have stored the right state.

    Here is a pretty detailed class diagram for the hub:

    ```plantuml
    class Hub {
        +__init__()
        +__str__()
        +add_room(room: Room)
        +get_room(name: str): Room
    }

    class Room {
        -name: str
        +__init__(name: str)
        +__str__()
        +get_name(): str
        +set_state(state: LightState)
        +store_scene(name: str)
        +activate_scene(name: str)
        +add_light(light: Light)
        +get_light(name: str): Light
    }

    note right {
        //~__str__()// example:
        **kitchen {**
          **table → l50**
          **ceiling → l75 h50 s99**
        **}**

        //store_scene// removes any scene by the
        given name, and creates a new scene
        based on the current light states

        //activate_scene// updates the current
        light state to match the stored 
        scene
    }

    class Scene {
        -name: str
        +__init__(name: str)
        +get_name(): str
        +add_light(Light)
        +activate()
    }

    note top of Scene {
        A //Scene// uses //SceneItem//s to store
        lights and the states they should get
        when the scene is activateed.

        //add_light// creates a new //SceneItem//,
        saving the light's current state as part
        of the scene.
    }

    Hub o--> "0.." Room
    Room o--> "0.." Scene
    Room o--> "0.." Light
    Scene *--> "0.." SceneItem

    SceneItem o-> LightState
    SceneItem -> Light

    class SceneItem {
        +__init__(light: Light, desired: LightState)
        +activate()
    }

    note top of SceneItem
        For a //Scene//, there is one //SceneItem//
        for each //Light// that is part of the //Scene//,
        connecting it to the desired //LightState// for
        that //Scene//.
    end note

    class Light {
        -name: str
        +__init__(name: str)
        +__str__()
        +get_name(): str
        +set_state(state: LightState)
        +get_state(): LightState
    }

    note right
        //~__str__()// example:
        **bedside → l75**

        The initial state for a //Light//
        should be //LightState(0)//, meaning
        it's off. 
    end note

    Light o--> LightState

    class ColorLight extends Light {
        +__str__()
    }
    note bottom of ColorLight
        Override //~__str__// in order to output
        the hue and saturation as well. Example:

        **ceiling → l75 h50 s99**
    end note

    class LightState {
        -level: int
        +__init__(level)
        +get_level(): int
        +get_hue(): int
        +get_saturation(): int
    }
    note left of LightState
        //~__str__()// example:
        **l75**

        //get_hue// and //get_saturation// should just
        return the default values (0) for a white light.
    end note


    class ColorLightState extends LightState {
        -hue: int
        -saturation: int
        +__init__(level, hue, saturation)
        +get_hue(): int
        +get_saturation(): int
    }
    ```

-
    0: Not a recognizable implementation of the class diagram.
    2: One or two largish mistakes implementing the class diagram.
    3: A few imperfection and code that is somewhat sloppy here and there.
    4: Perfect implementation using great OOP code.
    code: 0
    weight: 2
    text: |
        Within `lightlib.py` implement the library as described by the class diagram such that it runs `test.py` without any problems.

        *Hint:* To keep things concrete, we recommend that you gradually work through `test.py` from the top to the bottom, creating new library functionality as needed.

    
- Code quality:
    ^merge: codequality
    text: |
        OOP code quality guidelines:
        - Only use *private* attributes, with double underscores.
        - Methods that are only to be used internally by the class itself, should also be considered *private*.
        - Interact with instances of other classes not by reading/writing their attributes, but by calling a method, nicely asking the class to do something for you.
        - Put functionality in a method of the class that it (mostly) concerns. If it concerns multiple classes, see if you can split up the functionality.
        - Try to prevent creating (too many) setters and getters, as they imply the actual functionality acting on this class is being implemented outside of the class.
