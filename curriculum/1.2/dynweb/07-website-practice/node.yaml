name: Practice exam
description: Implement a secure dynamic web site, using HTML, CSS, Flask, SQLAlchemy and WTForms, based on a given set of wireframes.
days: 2
goals:
    flask: 1
    python-sql: 1
    orm: 1
    auth: 1
    web-security: 1
    web-forms: 1
assignment:
    Assignment: |
        The goal is to create an eating/cooking registration system ('kooklijst') for a student house, *Huize Brak*. 

        The students living in *Huize Brak* share a kitchen and a living/dining room. Instead of every person cooking just for her/himself, it is a lot more efficient and a lot more fun to eat together. The problem is: who will do the cooking, and how to keep track of grocery expenses? That's where your web app will come in!

    Functional requirements:
        -
            link: https://www.educative.io/answers/how-to-send-emails-with-api-in-flask-mail
            title: How to send emails with API in Flask-Mail
            info: How to use the `flask_mail` package to send emails from Python as if they were coming from your GMail account.
        -
            link: https://ethereal.email/
            title: Ethereal
            info: |
                As including your personal GMail credentials in your source code is a bad idea, and sending out actual emails from a development environment is equally questionable, we suggest an alternative option. Ethereal is a fake email service. You can create an account using just one click, which will get you the username, password and other settings needed to configure `flask_mail`. Emails sent through Ethereal are never actually delivered, but can be viewed on the Ethereal website, no matter what the *to*-address of the email was. Great for development!

                Note that (different from GMail in the article above), Ethereal requires `MAIL_USE_SSL = False` and `MAIL_USE_TLS = True`. Also, make sure your computer is *not* connected to the lab WiFi, but to EDUROAM, as the former will block outgoing emails.

        -
            title: Login by email
            ^merge: feature
            map:
                auth: 1
            text: |
                <wired-card>
                    <h1>Sign in</h1>
                    <label>User name</label>
                    <wired-input type="text" placeholder="User name" value="te@t"></wired-input>
                    <wired-card fill="#db0651" style="color: #fde9b9;">
                        <ul>
                        <li>User name can may only contain letters</li>
                        </ul>
                    </wired-card>
                    <div style="display: flex;">
                        <div style="flex: 1;"></div>
                        <wired-input type="submit" value="Email login link"></wired-input>
                    </div>
                </wired-card>
                <wired-card style="clear: both;">
                    <h1>Link sent</h1>
                    A login link has been sent to: <b>test@huizebrak.nl</b>
                </wired-card>

                Only the student living in *Huize Brak* should be allowed access to the system. Therefore, all requests (to other pages) should be redirected to a login page. As *Huize Brak* has its own domain name, `huizebrak.nl`, and all inhabitants have an email address at that domain (`joe@huizebrak.nl`), we can use that for authentication and authorization.

                The login form should only contain a single text input field for the user name (2 up to 20 characters; only letters, no special characters), and a submit button. On submit, an email containing an impossible to guess login-link (containing a randomly generated string) should be sent. The user should be shown the message that they need to check their email and click the link to login. Exactly like the LMS42 login procedure.
                
                The randomly generated part of the login links can be stored in the database, and may only be used once. When the link is first clicked, the browser's session should become associated with the user. If the user doesn't exist yet, it should be created.

        -
            title: Main page - the list of meals
            ^merge: feature
            map:
                flask: 1
                orm: 1
            text: |
                <wired-card>
                <h1>Meals</h1>
                <table>
                    <tr><th>Date</th><th>Cook</th><th>Food</th><th>#</th><th>€</th></tr>
                    <tr><td><a>2021-02-02</a></td><td></td><td></td><td>0</td><td></td></tr>
                    <tr><td><a>2021-02-01</a></td><td>Frank</td><td>Quiche</td><td>8</td><td>2.93</td></tr>
                    <tr><td><a>2021-01-31</a></td><td>Timothy</td><td>Nasi</td><td>5</td><td>3.22</td></tr>
                </table>
                </wired-card>

                On the main page, a list of the latest 14 meals should be displayed, ordered by date with the most recent on top. Each entry should show: date, cook name, meal description, number of participants (eaters) and price per participant.

                If no meal record exists for today, any empty record for the day (still without a cook, meal description or price) should be created automatically before rendering the main page.

                Clicking on a meal date should take the user to the *Meal page* (explained in the next objective).

                *Hints:*
                - You can use an [HTML table](https://developer.mozilla.org/en-US/docs/Learn/HTML/Tables/Basics) to layout the meals table.
                - There are two relationships between users and meals:
                    - The cook relationship. A meal can have 0 or 1 cook. A user can be a cook for any number of meals.
                    - The participant (eater) relationship. A meal can have any number of participants. A user can eat along with any number of meals. For the nice way of doing many-to-many relationships with Flask-SQLAlchemy, please refer to step 2 and 3 in the following [documentation](https://www.digitalocean.com/community/tutorials/how-to-use-many-to-many-database-relationships-with-flask-sqlalchemy#step-2-setting-up-database-models-for-a-many-to-many-relationship).

        -
            title: Meal page
            ^merge: feature
            map:
                web-forms: 1
                orm: 1
            text: |
                <wired-card>
                    <h1>Meal 2021-02-01</h1>
                    <table>
                    <tr><td>Cook</td><td>Frank</td></tr>
                    <tr><td>Participants</td><td><ul><li>Frank</li><li>Timothy</li></ul><wired-input type="submit" value="Unparticipate"></wired-input></td></tr>
                    </table>
                    <wired-card>
                    <h3>Declaration form</h3>
                    <label>Meal description</label><wired-input type="text"></wired-input>
                    <label>Total expenses</label><wired-input type="text"></wired-input>
                    <label>Upload receipt</label><wired-input type="submit" value="Browse.."></wired-input>
                    <div style="display: flex;">
                        <div style="flex: 1;"></div>
                        <wired-input type="submit" value="Submit"></wired-input>
                    </div>
                    </wired-card>
                </wired-card>
                <wired-card style="clear: both;">
                    <h1>Meal 2021-02-01</h1>
                    <table>
                    <tr><td>Description</td><td>Quiche</td></tr>
                    <tr><td>Cook</td><td>Frank</td></tr>
                    <tr><td>Total price</td><td>€23.40</td></tr>
                    <tr><td>Participants</td><td><ul><li>Frank</li><li>Timothy</li></ul></td></tr>
                    <tr><td>Receipt</td><td><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="10em" height="10em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M553.1 509.1l-77.8 99.2l-41.1-52.4a8 8 0 0 0-12.6 0l-99.8 127.2a7.98 7.98 0 0 0 6.3 12.9H696c6.7 0 10.4-7.7 6.3-12.9l-136.5-174a8.1 8.1 0 0 0-12.7 0zM360 442a40 40 0 1 0 80 0a40 40 0 1 0-80 0zm494.6-153.4L639.4 73.4c-6-6-14.1-9.4-22.6-9.4H192c-17.7 0-32 14.3-32 32v832c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V311.3c0-8.5-3.4-16.7-9.4-22.7zM790.2 326H602V137.8L790.2 326zm1.8 562H232V136h302v216a42 42 0 0 0 42 42h216v494z" fill="#626262"/><rect x="0" y="0" width="1024" height="1024" fill="rgba(0, 0, 0, 0)" /></svg></td></tr>
                    </table>
                </wired-card>

                The page for a meal should display all known details about that meal (including the names of all participants).

                If the meal doesn't have a cook yet, there should be a button that allows the user to sign up as a cook.

                In case the viewing user is the cook, a declaration form must be shown containing the following items:

                - Meal description (eg. 'Lasagna'), between 4 and 40 characters.
                - Total price of the groceries in EUR (eg. '15,23'), between 1 and 100 EUR.
                - Groceries receipt photo, required.

                Until this form has been submitted, users should be able to toggle whether or not they want to participate in the meal.

        -
            title: Main page - statistics
            ^merge: feature
            map:
                python-sql: 1
            text: |
                <wired-card>
                <h1>Meals</h1>
                <table>
                    <tr><th>Date</th><th>Cook</th><th>Food</th><th>#</th><th>€</th></tr>
                    <tr><td><a>2021-02-02</a></td><td></td><td></td><td>0</td><td></td></tr>
                    <tr><td><a href="#">2021-02-01</a></td><td>Frank</td><td>Quiche</td><td>8</td><td>2.93</td></tr>
                    <tr><td><a href="#">2021-01-31</a></td><td>Timothy</td><td>Nasi</td><td>5</td><td>3.22</td></tr>
                </table>
                <h1>Balances</h1>
                <table>
                    <tr><th>Name</th><th>Cooking</th><th>Expenses</th></tr>
                    <tr><td>Frank</td><td>-1</td><td>-5.77</td></tr>
                    <tr><td>Timothy</td><td>+1</td><td>+5.77</td></tr>
                </table>
                </wired-card>
                
                Besides the list of meals, the main page should show a list of inhabitants. For each it should display:

                - The user name. This can be just the part of the email address before the '@'-sign.
                - The *cooking balance* indicates how often a person cooks relatively to how often he/she participates in a meal. Every time a person participates in a meal, the balance goes down by 1. Every time a person cooks a meal, the balance goes up by the amount of participants.
                - The *expenses balance* indicates who owes/is to receive money. It works similarly to the *cooking balance*. When cooking, the total groceries price is added to your balance. When participating, the total price divided by the number of participants is subtracted from the balance.

                *Hint:* Create `get_cooking_balance()` and `get_expenses_balance()` methods on your `User` model class. You can call these methods directly from your Jinja template.
                <script src="/static/wired-elements.js"></script>

    Non-functional requirements:
        -
            title: Tech stack
            must: true
            text: |
                The functional requirements should be implemented using Python, Flask, SQLAlchemy and WTForms. JavaScript may not be used.

        -
            title: Security
            map:
                web-security: 1
            malus: 0.5
            0: Any hacker can gain full access to the system.
            1: Any hacker can gain read access to all information.
            2: People can view more information than they should (but not everything).
            3: One or two small security imperfections, with only minor impact.
            4: Flawless.
            text: |
                The web site should be secure. It should *not* be possible for anyone to take actions other than as described by the functional requirements.

        -
            title: Layout and styling
            0: No use of Jinja2 `extends`, looks terrible.
            2: No use of Jinja2 `extends`, looks fine.
            4: Proper use of Jinja2, looks fine.
            map:
                flask: 1
            text: |
                All pages should be consistently laid out and styled to a degree acceptable for an internal administration tool. All pages should share a common header and footer, implemented using a Jinja2 layout template.
