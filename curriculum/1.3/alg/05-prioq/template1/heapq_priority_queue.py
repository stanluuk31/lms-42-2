import heapq

class HeapqPriorityQueue:

    def __init__(self):
        self.__data = []

    def add(self, value) -> None:
        """Add `value` to the priority queue."""
        # TODO

    def fetch_smallest(self) -> int:
        """Find the smallest value in the priority queue, remove it from the
        queue and return it."""
        # TODO

    def is_empty(self) -> bool:
        """Returns `True` if the queue is empty, and `False` otherwise."""
        # TODO

    def length(self) -> int:
        """Returns the length of items in the queue"""
        # TODO