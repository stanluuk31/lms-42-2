# Company safari reflection

Company: ...


## Surprises

Name at least two things that were (a bit) different than you would have expected:

- ...
- ...


## Positives/negatives

Name at least three things about the company/day that you really liked, or didn't like.

- ...
- ...
- ...
