'use strict'

const open_record_button = document.getElementById("open_recorder")
const record_status = document.getElementById("record_status")
const time_ms_delay = 60000
const disable_button_text = "Recording window is open"
const enable_button_text = "Open recording tab"
let recorder_window;

function openRecorder() {
    recorder_window = window.open("/record", "_blank")
}

setInterval(function() {

    const last_screenshot = localStorage.getItem('lastScreenshot')

    let over_deadline = (last_screenshot && Date.now() - last_screenshot > time_ms_delay)
    let active_tab = Boolean(recorder_window && !recorder_window.closed)

    open_record_button.disabled = Boolean(active_tab || over_deadline != null && !over_deadline)
    
    if (open_record_button.disabled) {
        open_record_button.innerHTML = disable_button_text;
    } else {
        open_record_button.innerHTML = enable_button_text;
    }

    if (last_screenshot == null) {
        return
    } else if (over_deadline) {
        record_status.innerHTML = "❌";
    } else {
        record_status.innerHTML = "✅";
    }
}, 1000)
