from ..app import app
from ..routes.inbox import get_students
import flask

@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    fragment: str = flask.request.args.get('fragment', '')
    template = fragment if fragment == 'queue' else 'tv'

    exams_only = False
    if device in {'a', 'b', }:
        class_filter = f"ESD1V.{device}"
    elif device == 'd':
        class_filter = "DSD1V.a"
    elif device == 'iglo':
        class_filter = "E%"
        exams_only = True
    else:
        class_filter = None

    students = get_students(class_filter=class_filter, exams_only=exams_only) if class_filter else None
    
    return flask.render_template(template+'.html',
        header=False,
        device=device,
        students=students
    )
