from __future__ import annotations
import wtforms as wtf
import flask_wtf
from flask_login import current_user, login_required
import flask
import glob
import os
import openai
import re

from ..app import app, db
from ..models import curriculum
from ..models.ai import AIQuery
from ..models.attempt import Attempt
from ..assignment import Assignment


# - I have a generic question about the topic at hand. (1 credit)
# - I have a question about the assignment. (2 credits)
# - I need help getting my code to work. (3 credits)
# - I want feedback on my code. (4 credits)


class Form(flask_wtf.FlaskForm):
    question = wtf.TextAreaField('Question', description='Ask questions about the subject matter, the assignment or your code. You can include error messages here, or describe unexpected behavior. Leave empty in order to receive general feedback.')
    use_assignment = wtf.BooleanField('Look at the assignment', default=True, description='Increases cost.')
    use_submission = wtf.BooleanField('Look at my code', description='Use `lms upload` first. Increases cost.')
    use_solution = wtf.BooleanField('Look at teacher solution', description='Increases cost, and seems to confuse GPT sometime, providing feedback on the teacher solution.')
    use_template = wtf.BooleanField('Look at provided template', description='Increases cost. Doesn\'t seem to improve the output.')
    model = wtf.SelectField('OpenAI model', choices=[('gpt-3.5-turbo-1106', 'gpt-3.5-turbo-1106'), ('gpt-3.5-turbo-16k', 'gpt-3.5-turbo-16k (2x cost)'), ('gpt-4', 'gpt-4 (20x cost)'), ('gpt-4-32k', 'gpt-4-32k (40x cost)'), ('gpt-4-1106-preview', 'gpt-4-1106-preview (5x cost)')], default='gpt-4-1106-preview')
    submit = wtf.SubmitField('Perform magic', description="This can easily take over a minute. Please don't interrupt the browser.")


def read_files(attempt, subdir):
    pre = f"{attempt.directory}/{subdir}/"
    for filename in glob.glob(pre+"**", recursive=True):
        if not os.path.isfile(filename):
            continue
        assert filename.startswith(pre)
        short = filename[len(pre):]
        if '/data/' in filename or '/.' in filename or '/_' in filename or filename.split('.')[-1].lower() not in ('py','java','md','rs','js','sql',):
            continue

        with open(filename) as file:
            yield short, file.read()


AI_TUTOR_QUESTION = [
    {
        "role": "system",
        "content": "You are an AI tutor talking to a first year university Software Development student. The student has been given a training assignment. Your goal is to help the student by helping them figure out the answers to their questions themselves. You should never give away the answer, but instead provide hints and ask rhetorical questions that help the student learn.",
    },
]

AI_TUTOR_FEEDBACK = [
    {
        "role": "system",
        "content": "You are an AI tutor talking to a first year university Software Development student. The student has been given a training assignment. Your goal is to help the student improve her/his code by providing feedback. You must never output improved code yourself, but instead point out the problems, if necessary explain why that is a problem, and provide hints and ask rhetorical questions that lead the student towards a solution, and help the student learn.",
    },
]

PYTHON_FEEDBACK = """- Objective #1: Game set up
  - Crash when the user enters something other than a number as the number of players.
- Objective #2: Make a move
   - No issues found.
- Objective #3: Winner or Loser
  - The wrong player is indicated as the winner.
- Objective #4: Fancy board
  - The output has no color yet. Use the `blessed` library for this.
  - The output doesn't look right when printing a board size 6. Check your `if`/`else` logic.
- Objective #5: AI player
  - No issues found.
- Objective #6: Error handling
  - To do.
- Objective #7: Save game
  - To do.
- Objective #8: Code quality
  - Some functions are named as a *thing* instead of an *action*. For instance: `students_file` and `statistics`. Try to prefix a verb that expresses *what* the function does to the *thing*.
  - The `board` list and the `players` list both contain the information about which player owns which fields. Maintaining duplicate data extra work and easily leads to inconsistency bugs. Instead, consider creating a function like `get_board_from_players`, that give the `players` data returns the `board` data.
  - The code does not include empty lines, making it hard to read. Try to organize your code into short 'paragraphs', putting lines that are logically related right below each other, followed by an empty line. Optionally, you can start each paragraph with a comment line that act sort of like a section heading, briefly describing what's in the paragraph.
  - The `play_turn` function is rather long and deeply nested, harming readability. Consider splitting it up into multiple functions.
  - Your logic in the `play_turn` function can be simplified.
"""

EXAMPLE_CODE_QUESTION = [
    {
    "role": "system",
    "content": "What follows is an example question (from a different student, providing different code) and answer you might give.",
    },
    {
        "role": "user",
        "content": """When to start the game and I select the use of the fancy UI, I get the following error:
    ```
    Traceback (most recent call last):
    File "/home/frank/projects/lms42/x.py", line 2, in <module>
    player = players[player_index]
        ^^^^^^^^^^^^
    NameError: name 'player_index' is not defined
    ```""",
    },
    {
        "role": "assistant",
        "content": f"""The error message you're getting indicates that you're reading from variable `player_index`, but you haven't previously set the variable in this particular case. Ask yourself what happens in the `else` case of the `if use_fancy_ui` block.

{PYTHON_FEEDBACK}""",
    },
]

EXAMPLE_FEEDBACK = [
    {
    "role": "system",
    "content": "What follows is example feedback you might give (to a different student, providing different code):",
    },
    {
        "role": "assistant",
        "content": PYTHON_FEEDBACK,
    },
]

CLOSING_GENERAL = "Remember not to give away any answers, but to help the student in their learning process by providing hints and asking questions to help them understand. This is not a conversation, so don't ask questions expecting a response and don't offer further help."

FEEDBACK_INSTRUCTIONS = """For each objective, provide a bulleted sublist of issues with the student's code. This may include errors and deviations from the assignment requirements. Do NOT summarize the assignment. When work on the objective hasn't started yet, just use `- To do.` without further description as the bullet item. If the work was partially completed, briefly explain what still needs to be done. If the student has done additional work not required by the objective (but perhaps required by a later objective) don't mention it. For the objective regarding code quality, provide a detailed list of issues and hints for improving all of the code base. When an objective has no issues, just say `- No issues found.`. If multiple occurrences of the same problem are found, say so and don't repeat yourself. For each objective, output no more than 5 issues."""

CLOSING_FEEDBACK = [
    {
        "role": "system",
        "content": f"""Please provide the student with to-the-point bulleted feedback in Markdown. {FEEDBACK_INSTRUCTIONS}

Don't output anything except the bullet list of objectives. {CLOSING_GENERAL}""",
    },
]

CLOSING_QUESTION = [
    {
        "role": "system",
        "content": f"Below is the student's question. Please answer it in Markdown format. {CLOSING_GENERAL}",
    },
]

CLOSING_QUESTION_CODE = [
    {
        "role": "system",
        "content": f"""Below is the student's question. Please answer it in Markdown format. Also provide the student with to-the-point bulleted feedback in Markdown. {FEEDBACK_INSTRUCTIONS}

Don't output anything except your answer to the question and the bullet list of objectives. {CLOSING_GENERAL}""",
    },
]


def compile_messages(attempt, form):

    msgs = []

    if form.use_assignment.data:
        ao = Assignment.load_from_directory(attempt.directory)
        assignment = ao.to_ai_markdown().strip()
        assignment = re.subn(r'(?s)<script.*?</script>', '', assignment)[0]
        assignment = re.subn(r'(?s)<link.*?>', '', assignment)[0]
        msgs += [{
            "role": "system",
            "content": f"This is the Markdown-formatted assignment the student is working on:\n\n{assignment}",
        }]

    if form.use_template.data:
        for short, content in read_files(attempt, 'template'):
            msgs += [{
                "role": "system",
                "content": f"This was provided to the student as template file `{short}`:\n\n{content}",
            }]

    if form.use_solution.data:
        for solution_num, (short, content) in enumerate(read_files(attempt, 'solution')):
            if solution_num == 0:
                msgs += [{
                    "role": "system",
                    "content": "Below is a solution to the assignment created by a teacher. It's code quality should be considered good enough. You may use it the grasp the intention of the assignment, but may never share (parts of) it with the student. Note that different solutions may be just as good. Take care *not* to provide feedback on the teacher solution, but on the student solution.",
                }]
            msgs += [{
                "role": "system",
                "content": f"This is secret teacher solution file `{short}`:\n\n{content}",
            }]
    
    if form.use_submission.data:
        submitted = False
        for short, content in read_files(attempt, 'submission'):
            submitted = True
            msgs += [{
                "role": "user",
                "content": f"This is currently in my `{short}` file:\n\n{content}",
            }]
        if not submitted:
            form.use_submission.errors.append("You haven't submitted any files yet.")
            return None

    question = form.question.data.strip()
    if question:
        if form.use_submission.data:
            msgs = AI_TUTOR_QUESTION + EXAMPLE_CODE_QUESTION + msgs + CLOSING_QUESTION_CODE
        else:
            msgs = AI_TUTOR_QUESTION + msgs + CLOSING_QUESTION_CODE
        msgs += [{
            "role": "user",
            "content": question,
        }]
    else:
        if not form.use_submission.data:
            form.question.errors.append("You need to either ask a question or have the AI look at your code.")
            return None
        msgs = AI_TUTOR_FEEDBACK + EXAMPLE_FEEDBACK + msgs + CLOSING_FEEDBACK

    return msgs


@login_required
@app.route('/ai/queries/<int:query_id>', methods=['GET'])
def ai_retrieve(query_id: int):
    query = AIQuery.query.get(query_id)
    if not query:
        return "No such AI query", 404
    if not current_user.is_teacher and current_user.id != query.request_user_id:
        return "That's not your attempt", 403
    
    return flask.render_template('ai-answer.html', title=f"Answer {query_id}", query=query)


@login_required
@app.route('/ai', methods=['GET', 'POST'])
@app.route('/ai/<int:attempt_id>', methods=['GET', 'POST'])
def ai(attempt_id=None):
    if attempt_id:
        attempt = Attempt.query.get(attempt_id)
        if not attempt:
            return "No such attempt", 404
    
        if not current_user.is_teacher and attempt.student_id != current_user.id:
            return "That's not your attempt", 403
    else:
        attempt: Attempt = current_user.current_attempt
        
    if not attempt:
        flask.flash("No current attempt.")
    if current_user.ai_credits <= 0:
        flask.flash("No AI credits left.")
        attempt = None
    if attempt and attempt.credits and not current_user.is_teacher:
        flask.flash("AI cannot be used for exams")
        attempt = None

    form = None
    topic = None

    if attempt:
        node = curriculum.get("nodes_by_id").get(attempt.node_id)
        topic = node['name'] if node else attempt.node_id

        form = Form()
        if form.validate_on_submit():
            msgs = compile_messages(attempt, form)

            if msgs:
                # Temporarily deduct 1000 credits. We'll refund what's left after the query.
                current_user.ai_credits -= 1000
                db.session.commit()

                for msg in msgs:
                    print(f'## {msg["role"]}')
                    print(msg["content"])

                try:
                    completion: dict = openai.ChatCompletion.create(model=form.model.data, messages=msgs, temperature=0) # type: ignore
                except openai.OpenAIError as e:
                    flask.flash(f"OpenAI error: {e}")
                    current_user.ai_credits += 1000
                    db.session.commit()
                else:
                    choice = completion["choices"][0]
                    answer = choice["message"]["content"]
                    usage = completion["usage"]

                    ai_query = AIQuery(
                        topic=topic,
                        model=completion["model"],
                        messages=msgs,
                        request_user=current_user,
                        attempt=attempt,
                        answer=answer,
                        finish_reason=choice["finish_reason"],
                        prompt_tokens=usage["prompt_tokens"],
                        completion_tokens=usage["completion_tokens"],
                    )
                    db.session.add(ai_query)
                    current_user.ai_credits += 1000 - ai_query.milli_dollars
                    db.session.commit()

                    return flask.redirect(f"/ai/queries/{ai_query.id}")

    history = AIQuery.query
    if not current_user.is_teacher:
        history = history.filter_by(request_user_id=current_user.id)
    history = history.order_by(AIQuery.id.desc()).limit(25)

    return flask.render_template('ai.html', form=form, history=history, topic=topic)
