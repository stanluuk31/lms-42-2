import datetime
from collections import defaultdict
from .utils import local_to_utc, utc_to_local

START_TIME = {"hour": 8, "minute": 30, "second": 0, "microsecond": 0}
END_TIME = {"hour": 17, "minute": 0, "second": 0, "microsecond": 0}
DAY_HOURS = END_TIME["hour"] - START_TIME["hour"] + (END_TIME["minute"] - START_TIME["minute"])/60

DEADLINE_TIME = {"hour": 17, "minute": 0, "second": 0, "microsecond": 0}

VACATIONS = [
    # 2020/2021
    ('2020-10-12', '2020-10-16'),
    ('2020-12-21', '2021-01-01'),
    ('2021-02-22', '2021-02-26'),
    '2021-04-02',
    '2021-04-05',
    '2021-04-27',
    ('2021-05-03', '2021-05-07'),
    '2021-05-13',
    '2021-05-14',
    '2021-05-24',
    ('2021-07-19', '2021-08-27'),

    # 2021/2022
    ('2021-08-30', '2021-08-31'), # sd intro
    ('2021-10-18', '2021-10-22'),
    ('2021-12-27', '2022-01-07'),
    ('2022-02-21', '2022-02-25'),
    '2022-04-15',
    '2022-04-18',
    '2022-04-27',
    ('2022-05-02', '2022-05-06'),
    '2022-05-26',
    '2022-05-27',
    '2022-06-06',
    ('2022-07-18', '2022-09-02'),

    # 2022/2023
    '2022-09-05', # sd intro
    ('2022-10-17', '2022-10-21'), # herfst
    ('2022-12-26', '2023-01-06'), # kerst
    ('2023-02-27', '2023-03-03'), # voorjaar
    '2023-04-07', # goede vrijdag
    '2023-04-10', # 2e paasdag
    ('2023-04-27', '2023-04-28'), # koningsdag en brugdag
    ('2023-05-01', '2023-05-05'), # mei
    ('2023-05-18', '2023-05-19'), # hemelvaart
    '2023-05-29', # pinkster
    ('2023-07-17', '2023-07-21'), # early vacation due to expiring student travel cards
    ('2023-07-24', '2023-09-01'), # zomer+hoi

    # 2023/2024
    '2023-09-04', # sd intro
    ('2023-10-23', '2023-10-27'), # herfst
    ('2023-12-25', '2024-01-05'), # kerst
    ('2024-02-19', '2024-02-23'), # voorjaar
    '2024-03-29', # goede vrijdag
    '2024-04-01', # 2e paasdag
    ('2024-04-29', '2024-05-03'), # mei
    ('2024-05-09', '2024-05-10'), # hemelvaart
    '2024-05-20', # pinkster
    ('2024-07-15', '2024-07-19'), # week 41 (non-academic 4.11) office open but not required (due to OV card)
    ('2024-07-22', '2024-08-30'), # zomer+hoi

    # 2024/205
    # intro: to be determined
    ('2024-10-28', '2024-11-01'), # herfst
    ('2024-12-23', '2025-01-03'), # kerst
    ('2025-02-17', '2025-02-21'), # voorjaar
    '2025-04-18', # goede vrijdag
    '2025-04-21', # 2e paasdag
    '2025-04-27', # koningsdag
    ('2025-04-28', '2025-05-02'), # mei
    '2025-05-05', # bevreidingsdag
    ('2025-05-29', '2025-05-30'), # hemelvaart
    '2025-06-09', # pinkster
    ('2025-07-14', '2025-07-18'), # week 41 (academic 4.12) office open but not required (due to OV card)
    ('2025-07-21', '2025-09-01'), # zomer+hoi
]



def calculate_vacation_days():
    days = set()
    for item in VACATIONS:
        if isinstance(item, str):
            days.add(datetime.date.fromisoformat(item))
        else:
            date = datetime.date.fromisoformat(item[0])
            end_date = datetime.date.fromisoformat(item[1])
            while date <= end_date:
                days.add(date)
                date += datetime.timedelta(days=1)
    return days

# Precalculate a set containing all vacation days
VACATION_DAYS = calculate_vacation_days()

# The last academic year for which we know the dates
MAX_ACADEMIC_YEAR = max(VACATION_DAYS).year - 1


def is_working_week(date: datetime.date):
    monday = date + datetime.timedelta(days = (7 - date.weekday()) % 7)
    for offset in range(0,5):
        if (date + datetime.timedelta(days=offset)) not in VACATION_DAYS:
            return True
    return False


def is_working_day(date: datetime.date):
    return date.weekday() not in [5,6] and date not in VACATION_DAYS


def offset(date: datetime.date, days: int):
    while not is_working_day(date):
        date += datetime.timedelta(days=1)

    offset = 1
    if days < 0:
        offset = -1
        days = -days

    for _ in range(days):
        date += datetime.timedelta(days=offset)
        while not is_working_day(date):
            date += datetime.timedelta(days=offset)
    return date


def offset_hours(time: datetime.datetime, working_hours: int, ignore_dates: set = set()):
    """Given a start `time` in timezone naive UTC, return an end time that is `working_hours`
    working hours later, skipping any dates in `ignore_dates`."""

    # We'll work with local times, and convert the result back to UTC.
    time = utc_to_local(time)
    if time.time() < datetime.time(**START_TIME):
        time = time.replace(**START_TIME)

    while True:
        date = time.date()
        if is_working_day(date) and date not in ignore_dates:
            # It's a working day. Calculate hours til end of day.
            delta_hours = max(0, (time.replace(**END_TIME) - time).total_seconds() / 60 / 60)
            if delta_hours >= working_hours:
                # This is the last day.
                time += datetime.timedelta(hours=working_hours)
                # Convert the result back to UTC.
                return local_to_utc(time)

            working_hours -= delta_hours

        # Move on to the next day.
        time += datetime.timedelta(days=1)
        time = time.replace(**START_TIME)


def calculate_per_month(date: datetime.date, end_date: datetime.date = datetime.date.today()):
    result = defaultdict(int)
    while date < end_date:
        if is_working_day(date):
            result[f"{date.year}-{date.month:02}"] += 1
        date += datetime.timedelta(days=1)
    return dict(result)


def get_working_hours_delta(start: datetime.datetime, end: datetime.datetime, ignore_dates: set = set()):
    """Get the number of (fractional) working hours between `start` and `end`,
    both in timezone naive UTC. Will never return less than 0.""" 
    start = utc_to_local(start)
    end = utc_to_local(end)
    start_time = datetime.time(**START_TIME)
    end_time = datetime.time(**END_TIME)

    if start.time() < start_time:
        start = start.replace(**START_TIME)
    if start.time() > end_time:
        start = start.replace(**END_TIME)
    if end.time() > end_time:
        end = end.replace(**END_TIME)

    seconds = 0
    while start < end:
        if is_working_day(start.date()) and start.date() not in ignore_dates:
            day_end = end if start.date()==end.date() and end.time() < end_time else start.replace(**END_TIME)
            seconds += max(0, (day_end - start).seconds)

        start = (start + datetime.timedelta(days=1)).replace(**START_TIME)

    return seconds / 3600


def calculate_week_starts():
    week_starts = {}
    monday = datetime.date(2020, 8, 31) # The start of the 2020/2021 academic year.
    last_vacation_day = sorted(VACATION_DAYS)[-1] + datetime.timedelta(days=7)
    week = 1
    year = 2020
    while monday < last_vacation_day:
        if is_working_week(monday):
            week_starts.setdefault(year, {})
            week_starts[year][week] = monday
            week += 1

        monday += datetime.timedelta(days=7)
        if monday.month >= 8 and year < monday.year:
            year = monday.year
            week = 1
    return week_starts

def print_week_starts():
    for year, weeks in calculate_week_starts().items():
        print(year)
        for week, start in weeks.items():
            print(f"  {week}: {start}")


# Precalculate the Monday dates of the education week numbers per year
WEEK_STARTS = calculate_week_starts() # {year: {week: monday_date}}


def get_quarter_dates(year, quarter, q4="whole_year"):
    """Returns a tuple with the first day of the quarter and day after the quarter specified 
    by `year` and `quarter`. When `year` is 2021 and `quarter` is 4, it would return the
    dates of the last quarter of the 2021/2022 academic year.
    
    :param year: The academic year. (So 2021 for the 2021/2022 year.)
    :param quarter: The quarter (1-4).
    :param q4: How to calculate the end date for the 4th period.
        "whole_year": there is no gap between q4 and q1, with the transition being 2 weeks before
            the strict start of q1. (default)
        "strict": until the first date of 'week 11'
        "extra_week": until the first date of 'week 12'
    """
    return get_period_dates(year, quarter, 10, q4)


def get_octant_dates(year, octant, q4="whole_year"):
    """Returns a tuple with the first day of the octant and day after the octant specified 
    by `year` and `octant`. When `year` is 2021 and `octant` is 8, it would return the
    dates of the last quarter of the 2021/2022 academic year.
    
    :param year: The academic year. (So 2021 for the 2021/2022 year.)
    :param octant: The octant (1-8).
    :param q4: How to calculate the end date for the 4th period.
        "whole_year": there is no gap between q4 and q1, with the transition being 2 weeks before
            the strict start of q1. (default)
        "strict": until the first date of 'week 6'
        "extra_week": until the first date of 'week 7'
    """
    return get_period_dates(year, octant, 5, q4)


def get_period_dates(year, period, period_weeks, q4="whole_year"):
    start = WEEK_STARTS[year][(period-1) * period_weeks + 1]
    end = WEEK_STARTS[year].get(period * period_weeks + 1, WEEK_STARTS[year][period * period_weeks] + datetime.timedelta(days=7))
    if period == 40//period_weeks:
        if q4 == "whole_year":
            end = WEEK_STARTS[year+1][1] + datetime.timedelta(days=-14)
        elif q4 == "extra_week":
            if is_working_week(end):
                end += datetime.timedelta(days=7)
        elif q4 != "strict":
            raise ValueError("Invalid q4 parameter")
    elif period == 1 and q4 == "whole_year":
        start += datetime.timedelta(days=-14)
    return start, end


def get_current_quarter(day=None):
    if day==None:
        day = datetime.date.today()
    for year in (day.year-1, day.year):
        for quarter in [1,2,3,4]:
            start,end  = get_quarter_dates(year, quarter)
            if start <= day < end:
                return year, quarter
    raise Exception("Couldn't determine quarter")


def get_current_octant(day=None):
    if day==None:
        day = datetime.date.today()
    for year in (day.year-1, day.year):
        for octant in range(1,9):
            start,end  = get_octant_dates(year, octant)
            if start <= day < end:
                return year, octant
    raise Exception("Couldn't determine octant")


def date_range(start, end):
    day = offset(start, 0)
    while day < end:
        yield day
        day = offset(day, +1)


if __name__ == "__main__":
    # poetry run python -m lms42.working_days  
    print_week_starts()
    for year in range(2020, MAX_ACADEMIC_YEAR+1):
        start, _ = get_quarter_dates(year, 1, "extra_week")
        _, end = get_quarter_dates(year, 4, "extra_week")
        day_count = 0
        for day in date_range(start, end):
            if is_working_day(day):
                day_count += 1
        print(f"{year}: {start} - {end} --> {day_count} working days")
