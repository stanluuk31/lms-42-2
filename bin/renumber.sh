
#!/bin/sh

cnt=0
for old in `ls -1 | grep -E '^[0-9]{2,3}-' | LANG=C sort` ; do
	cnt=$((cnt+1))
	base=`echo "$old" | cut -d - -f 2-`
	new="`seq -f "%02g" $cnt $cnt`-$base"
	if [ "$old" != "$new" ] ; then
		echo mv "$old" "$new"
		mv "$old" "$new"
	else
		echo keep "$old"
	fi
done
